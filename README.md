### Добавление пакета:

`composer require antil-php-packages/yii-file-service:0.*`
, где 0.* требуемая версия

### Действия по интеграции в проект:

* добавить модуль в проект (console): 
```
'modules' => [
    'fileService'  => [
        'class' => FileServiceModule::class,
    ],
]
```

* добавить модуль в автозагрузку (console):
```
'bootstrap' => ['fileService'],
```
