SHELL = /bin/sh

package-folder:=yii-file-service
docker := $(shell command -v docker 2> /dev/null)
docker-compose:= $(shell command -v docker-compose 2> /dev/null)

php := $(docker) exec -w /var/www/antil-php-packages/$(package-folder) antil-php-packages
php_tty := $(docker) exec -it -w /var/www/antil-php-packages/$(package-folder) antil-php-packages

sh := $(php_tty) bash
yii := $(php) php yii
composer := $(php) composer -o
php-cs-fixer := $(php) vendor/bin/php-cs-fixer

sh:
	$(sh)

composer-i:
	$(composer) i -o

composer-u:
	$(composer) u -o

fixer:
	$(php-cs-fixer) --verbose --using-cache=no fix --config=.php_cs.dist.php
